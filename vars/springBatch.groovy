#!/usr/bin/env groovy

def call(String service, String profile, String jobName = 'sampleJob', Boolean isSendSuccess = true, Map<String, String> params, String nodeName = 'slave') {
    print("service=$service, profile=$profile, jobName=$jobName, params=$params")

    def notifier = new kr.doctornow.notifier()
    def tag = 'latest'
    def prefix = profile == 'production' ? '' : 'dev-'
    def ecrName = "$prefix$service"
    def accountId = 170360853218
    def ecrUrl = "${accountId}.dkr.ecr.ap-northeast-2.amazonaws.com"
    def url = "${ecrUrl}/${ecrName}:${tag}"

    timeout(time: 30, unit: 'MINUTES') {
        node(nodeName) {
            try {
                stage('Login') {
                    
                    withAWS(credentials: 'doctornow-aws') {
                        sh "aws ecr get-login-password --region ap-northeast-2 | docker login --username AWS --password-stdin ${ecrUrl}"
                    }
                }
                
                stage('Pull Image') {
                    sh "docker pull $url"
                }
                
                stage('Run') {
                    def parameters = ''
                    if (params != null) {
                        params.each {
                            parameters += "\'-${it.key}=${it.value}\' "
                        }
                        if (parameters.length() > 0) parameters = parameters[0..-2]
                    }

                    withCredentials([[
                        $class: 'AmazonWebServicesCredentialsBinding',
                        credentialsId: "doctornow-aws",
                        accessKeyVariable: 'AWS_ACCESS_KEY_ID',
                        secretKeyVariable: 'AWS_SECRET_ACCESS_KEY'
                    ]]) {
                        sh "docker run -e AWS_ACCESS_KEY_ID=\"${AWS_ACCESS_KEY_ID}\" -e AWS_SECRET_ACCESS_KEY=\"${AWS_SECRET_ACCESS_KEY}\" ${url} --spring.batch.job.names=${jobName} ${parameters}"
                    }
                    
                    if (isSendSuccess) {
                        notifier.notifyBatchToSlack(profile, 'SUCCESS', 'good')
                    }
                }
            } catch (e) {
                notifier.notifyBatchToSlack(profile, 'FAILED', '#FF0000')
                throw e
            }
        }
    }
}