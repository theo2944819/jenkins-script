#!/usr/bin/env groovy

def call(String repoUrl, String service, String profile, String branch = null, Boolean isMonoRepo = false, Boolean isSendDeployShare = true, String serviceSuffix = '', String nodeName = 'slave') {
    def notifier = new kr.doctornow.notifier()

    def ecrRepositoryUrl = "170360853218.dkr.ecr.ap-northeast-2.amazonaws.com"
    def targetBranch = branch == null || branch == '' ? "${service}-devzone" : branch
    def prefix = profile == 'production' ? '' : 'dev-'
    def interfaceModuleName = isMonoRepo ? "${service}-interface" : 'interface'
    def domainModuleName = isMonoRepo ? "${service}-domain" : 'domain'
    def interfacePrefix = isMonoRepo ? "$service/" : ''
    def ecrServiceRepositoryUrl = "$ecrRepositoryUrl/$prefix$service"
    def ecsCluster = profile == 'production' ? 'production' : 'dev-doctornow-cluster'
    def ecsService = "$prefix$service$serviceSuffix"
    def imageTag = "${profile}-${env.BUILD_NUMBER}"

    print("parameters - service=$service, profile=$profile, targetBranch=$targetBranch, isMonoRepo=$isMonoRepo")

    def TASK_DEFINITION_NAME = "$prefix$service-task"
    def TASK_DEFINITION_PATH = "/var/lib/jenkins/workspace/$env.JOB_NAME/${interfacePrefix}interface/${prefix}task-definition.json"
    def AWS_REGION = 'ap-northeast-2'
    def customBuildName = "#${BUILD_NUMBER} ${isMonoRepo != null && isMonoRepo ? service : ''}"

    node(nodeName) {
        buildName customBuildName
        
        try {
            stage('Checkout and Env Setting') {
                def commitEnv = checkout([$class: 'GitSCM', branches: [[name: "*/${targetBranch}"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'jiho0707', url: repoUrl]]])
                customBuildName = "${customBuildName} ${commitEnv.GIT_COMMIT[0..7]}"
                buildName customBuildName
                env.GIT_COMMIT_MSG = sh (script: 'git log -1 --pretty=%B ${GIT_COMMIT}', returnStdout: true).trim()
                if(profile == 'production' && isSendDeployShare) {
                    slackSend(channel: "공지_배포공유", color: "good", message: "[$service] 다음 배포가 시작됩니다.\n$env.GIT_COMMIT_MSG")
                }
            }
            
            stage('Jib Build') {
                withAWS(credentials: 'doctornow-aws') {
                    withCredentials([usernamePassword(credentialsId: 'drnow-github-package', passwordVariable: 'GITHUB_TOKEN', usernameVariable: 'GITHUB_USERNAME')]) {
                        script {
                            try {
                                sh "./gradlew $domainModuleName:clean"
                            } catch(e) { 
                                print("domain 모듈이 없나 봄")
                            }
                            sh "./gradlew $interfaceModuleName:clean"
                            sh "./gradlew -PimageTag=$imageTag -Pstage=$profile -PserviceName=${service} $interfaceModuleName:copyDatadogAgent $interfaceModuleName:jib"
                        }
                    }
                }
            }
            
            stage('ECS deploy') {
                withAWS(credentials: 'doctornow-aws') {
                    script {
                        sh "sed -i \"s@${ecrServiceRepositoryUrl}.*@${ecrServiceRepositoryUrl}:${profile}-${env.BUILD_NUMBER}\\\",@g\" $TASK_DEFINITION_PATH"
                        def taskRevision = sh(script: "aws ecs register-task-definition --cli-input-json file://${TASK_DEFINITION_PATH} --region ${AWS_REGION} | jq --raw-output .taskDefinition.revision", returnStdout: true).trim() as Integer

                        customBuildName = "$customBuildName task:$taskRevision"
                        buildName customBuildName

                        print("test log, imageTag=$imageTag, taskRevision=$taskRevision")
                        notifier.notifyDeployStartToSlack(service, profile, imageTag)

                        sh "aws ecs update-service --cluster $ecsCluster --service $ecsService --task-definition ${TASK_DEFINITION_NAME}:${taskRevision} --region ${AWS_REGION}"
                        sh "aws ecs wait services-stable --cluster $ecsCluster --services $ecsService --region ${AWS_REGION}"
                    }
                }
            }
            
            notifier.notifyDeployToSlack(service, profile, 'SUCCESS', 'good')
        } catch (e) {
            notifier.notifyDeployToSlack(service, profile, 'FAILED', '#FF0000')
            throw e
        }
    }
}
